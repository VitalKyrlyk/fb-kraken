package com.example.fbkraken.Dagger.module;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.fbkraken.MyApp;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {

    @Provides
    @Singleton
    Context provideContext (MyApp applicaton){
        return applicaton.getApplicationContext();
    }

    @Provides
    @Singleton
    SharedPreferences provideSharedPreferences (Context context){
        return context.getSharedPreferences("score", Context.MODE_PRIVATE);
    }

}
