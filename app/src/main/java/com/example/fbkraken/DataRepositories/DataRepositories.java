package com.example.fbkraken.DataRepositories;

import android.content.SharedPreferences;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class DataRepositories {

    private SharedPreferences sharedPreferences;

    @Inject
    DataRepositories (SharedPreferences sharedPreferences){
        this.sharedPreferences = sharedPreferences;
    }

    public void putData(String key, int data){
        sharedPreferences.edit().putInt(key, data).apply();
    }

    public int getData(String key){
      return sharedPreferences.getInt(key, 0);
    }

}
