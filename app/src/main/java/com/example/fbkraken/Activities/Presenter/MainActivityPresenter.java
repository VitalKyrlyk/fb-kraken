package com.example.fbkraken.Activities.Presenter;

import android.widget.ImageButton;

import com.example.fbkraken.Activities.View.MainActivityView;
import com.example.fbkraken.DataRepositories.DataRepositories;
import com.example.fbkraken.R;

import java.util.ArrayList;
import java.util.Random;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class MainActivityPresenter {

    private DataRepositories dataRepositories;
    private MainActivityView view;
    private static String RECORD_KEY = "record";

    @Inject
    MainActivityPresenter (DataRepositories dataRepositories){
        this.dataRepositories = dataRepositories;
    }

    public void getView(MainActivityView view){
        this.view = view;
    }

    public void putData(String key, int data){
        dataRepositories.putData(key,data);
    }

    public int getData(String key){
       return dataRepositories.getData(key);
    }

    public void showScore(String key){
        view.showScore(getData(key));
    }

    private void recordScore(int score){
        if (score > getData(RECORD_KEY)){
            putData(RECORD_KEY, score);
            view.showRecord(score);
        }
    }

    public ImageButton getRandomCell(ArrayList<ImageButton> buttons){

        Random random = new Random();
        int cell = random.nextInt(9);

        return buttons.get(cell);

    }

    public Integer getRandomCoin(){
        ArrayList<Integer> images =new ArrayList<>();
        images.add(0, R.drawable.gold_coin);
        images.add(1, R.drawable.silver_coin);
        images.add(2, R.drawable.bronze_coin);
        Random random = new Random();
        int coin = random.nextInt(3);

        return images.get(coin);

    }

    public void changeScore(Integer coin, String key){
        int goldCoin = R.drawable.gold_coin;
        int silverCoin = R.drawable.silver_coin;
        int bronzeCoin = R.drawable.bronze_coin;
        int goldPoint = 2;
        int silverPoint = 1;
        int bronzePoint = -3;

        int score = 0;

        if (coin.equals(goldCoin)){
            score = goldPoint;
        } else if (coin.equals(silverCoin)){
            score = silverPoint;
        } else if (coin.equals(bronzeCoin)){
            score = bronzePoint;
        }

        score += getData(key);
        putData(key, score);
        showScore(key);
        recordScore(score);
    }

}
