package com.example.fbkraken.Activities.View;

public interface MainActivityView {

    void showScore(int score);

    void showRecord(int record);

}
