package com.example.fbkraken.Activities;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.fbkraken.Activities.Fragment.LoseDialogFragment;
import com.example.fbkraken.Activities.Presenter.MainActivityPresenter;
import com.example.fbkraken.Activities.View.MainActivityView;
import com.example.fbkraken.R;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import dagger.android.support.DaggerAppCompatActivity;

public class MainActivity extends DaggerAppCompatActivity implements MainActivityView {

    @BindView(R.id.table)
    TableLayout tableLayout;

    @BindView(R.id.cellIb1)
    ImageButton cellIb1;

    @BindView(R.id.cellIb2)
    ImageButton cellIb2;

    @BindView(R.id.cellIb3)
    ImageButton cellIb3;

    @BindView(R.id.cellIb4)
    ImageButton cellIb4;

    @BindView(R.id.cellIb5)
    ImageButton cellIb5;

    @BindView(R.id.cellIb6)
    ImageButton cellIb6;

    @BindView(R.id.cellIb7)
    ImageButton cellIb7;

    @BindView(R.id.cellIb8)
    ImageButton cellIb8;

    @BindView(R.id.cellIb9)
    ImageButton cellIb9;

    @BindView(R.id.scoreTv)
    TextView scoreTv;

    @BindView(R.id.recordTv)
    TextView recordTv;

    @BindView(R.id.btnStart)
    Button btnStart;

    @OnClick(R.id.btnStop)
    void btnStop(){
        stopGame();
    }

    @OnClick(R.id.btnStart)
    void btnStart(){
        startGame();
        btnStart.setClickable(false);
    }

    @Inject
    MainActivityPresenter presenter;

    static String SCORE_KEY = "score";
    static String RECORD_KEY = "record";

    ArrayList<ImageButton> buttons;
    Timer timer;
    TimerTask timerTask;
    ImageButton imageButtonCell;
    MediaPlayer mediaPlayer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        presenter.getView(this);
        showScore(presenter.getData(SCORE_KEY));
        showRecord(presenter.getData(RECORD_KEY));

        createList();

    }

    @Override
    public void showScore(int score) {
        scoreTv.setText(String.valueOf(score));
        if (score < 0) {
            stopGame();
            LoseDialogFragment loseDialogFragment = new LoseDialogFragment();
            Bundle bundle = new Bundle();
            bundle.putInt("score", score);
            loseDialogFragment.setArguments(bundle);
            loseDialogFragment.show(getSupportFragmentManager(), "Dialog");
            presenter.putData(SCORE_KEY, 0);
            scoreTv.setText(String.valueOf(0));
        }
    }

    @Override
    public void showRecord(int record) {
        recordTv.setText(String.valueOf(record));
    }

    public void createList(){
        buttons = new ArrayList<>();
        buttons.add(0, cellIb1);
        buttons.add(1, cellIb2);
        buttons.add(2, cellIb3);
        buttons.add(3, cellIb4);
        buttons.add(4, cellIb5);
        buttons.add(5, cellIb6);
        buttons.add(6, cellIb7);
        buttons.add(7, cellIb8);
        buttons.add(8, cellIb9);
    }

    public void startGame(){

        mediaPlayer = MediaPlayer.create(this, R.raw.melodi);
        mediaPlayer.start();

        timer = new Timer();
        timerTask = new TimerTask() {

            @Override
            public void run() {

                if (!mediaPlayer.isPlaying()) mediaPlayer.start();

                runOnUiThread(() -> {

                    imageButtonCell =  presenter.getRandomCell(buttons);
                    Integer coin = presenter.getRandomCoin();
                    imageButtonCell.setScaleType(ImageView.ScaleType.FIT_CENTER);

                    Glide.with(getApplicationContext())
                            .load(coin)
                            .into(imageButtonCell);

                    imageButtonCell.animate().rotation(360f).start();
                    imageButtonCell.setClickable(true);
                    onClickCoin(imageButtonCell, coin);

                    TimerTask secondTask = new TimerTask() {

                        @Override
                        public void run() {

                            runOnUiThread(() -> {

                                imageButtonCell.animate().rotation(-360f).start();
                                Glide.with(getApplicationContext())
                                        .load(R.color.background)
                                        .into(imageButtonCell);
                                imageButtonCell.setClickable(false);

                            });
                        }
                    };

                    timer.schedule(secondTask, 1000);
                });
            }
        };

        timer.schedule(timerTask, 1000, 2000);
    }

    public void onClickCoin(final ImageButton button, final Integer coin){
        button.setOnClickListener(v -> {
            button.setClickable(false);
            presenter.changeScore(coin, SCORE_KEY);
        });
    }

    public void stopGame(){
        if (timer != null) {

            if (mediaPlayer != null && mediaPlayer.isPlaying()) mediaPlayer.stop();
            timer.cancel();
            timer = null;
            imageButtonCell.setClickable(false);
            Glide.with(getApplicationContext())
                    .load(R.color.background)
                    .into(imageButtonCell);
            btnStart.setClickable(true);

        } else {
            presenter.putData(SCORE_KEY, 0);
            presenter.showScore(SCORE_KEY);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopGame();
    }
}
