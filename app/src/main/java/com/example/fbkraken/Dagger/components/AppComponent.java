package com.example.fbkraken.Dagger.components;


import com.example.fbkraken.Dagger.ActivityBuilder;
import com.example.fbkraken.Dagger.module.AppModule;
import com.example.fbkraken.MyApp;

import javax.inject.Singleton;

import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;

@Singleton
@Component(modules = {
        AndroidSupportInjectionModule.class,
        AppModule.class,
        ActivityBuilder.class})

public interface AppComponent extends AndroidInjector<MyApp> {

    @Component.Builder
    abstract class Builder extends AndroidInjector.Builder<MyApp>{

    }

}
