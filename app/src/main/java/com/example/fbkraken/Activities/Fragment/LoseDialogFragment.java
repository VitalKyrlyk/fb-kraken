package com.example.fbkraken.Activities.Fragment;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.example.fbkraken.R;

import java.util.Objects;

public class LoseDialogFragment extends AppCompatDialogFragment {

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(Objects.requireNonNull(getActivity()));
        LayoutInflater inflater = getActivity().getLayoutInflater();
        @SuppressLint("InflateParams") View view = inflater.inflate(R.layout.fragmentdialog_lose, null);

        TextView loseTvScore = view.findViewById(R.id.loseTvScore);
        Bundle arg = getArguments();
        String score = String.valueOf(Objects.requireNonNull(arg).getInt("score"));
        loseTvScore.setText(score);

        builder.setView(view);

        return builder.create();
    }
}
